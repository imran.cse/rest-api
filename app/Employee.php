<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  protected $table='employees';
  public $fillable=['name','phone','address'];
}
