@extends('layouts.backend')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-xl-8 mx-auto">

                <form method="post" action="{{route('employee.store')}}">
                    @csrf
                <div class="block block-themed">
                    <div class="block-header bg-info">
                        <h3 class="block-title">Add Employee</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                        </div>
                    </div>
                    <div class="block-content">
                       <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        <input type="text" class="form-control" id="name" name="name">
                                        <label for="name">Enter Name</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-material floating">
                                            <input type="text" class="form-control" id="phone" name="phone">
                                            <label for="roll">Enter Phone No:</label>

                                        </div>
                                        
                                    </div>
                                </div>



                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="form-material floating">
                                            <input type="text" class="form-control" id="address" name="address">
                                            <label for="registration_no">Enter Address</label>
                                        </div>

                                    </div>
                                </div>
                            </div>



                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-alt-info">
                                        <i class="fa fa-send mr-5"></i>Submit
                                    </button>
                                </div>
                            </div>

                    </div>
                </div>
                </form>



    </div>
        </div>
    </div>
</div>



@endsection
