@extends('layouts.backend')

@section('content')
<div class="content">
	<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Users Table</h3>
        </div>
        <div class="block-content block-content-full">

            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            	<div class="row">

            		<div class="col-sm-12 col-md-12">
            			<a class="float-right" href="/employee/create" >
            				<button class="btn btn-success">Add Employee</button>
            			</a>
            		</div>
            	</div><br>

	            <div class="row">
		            <div class="col-sm-12">
		            	<table id="example" class="display nowrap" cellspacing="0"style="width:100%">
					        <thead>
					            <tr>
					                <th>SL</th>
			                    	<th>Name</th>
			                    	<th>Email</th>
			                    	<th>Action</th>
					            </tr>
					        </thead>
					        <tbody>

								@foreach ($users as $user)
					            <tr>
					                <td>{{ $user->id }}</td>
			                        <td>{{ $user->name }}</td>
			                        <td>{{ $user->email }}</td>

			                        <td>
			                        	<a class="btn btn-success mr-5 mb-5" href="#">
	                                    <i class="fa fa-pencil-square-o text-white mr-5"></i> 
	                                	</a>
	                                    <a class="btn btn-info mr-5 mb-5" href="#">
	                                        <i class="fa fa-check-circle text-white mr-5"></i> 
	                                    </a>

	                                    <a class="btn btn-danger mr-5 mb-5" href="#">
	                                        <i class="fa fa-trash-o text-white mr-5"></i> 
	                                    </a>
				                    </td>
					            </tr>
					            @endforeach
					        </tbody>

   					 	</table>
		        	</div>
	        	</div>
			</div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#example').DataTable({
    		"scrollX": true

    	});
	} );
</script>



@endsection
